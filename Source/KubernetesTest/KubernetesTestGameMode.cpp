// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "KubernetesTestGameMode.h"
#include "KubernetesTestHUD.h"
#include "KubernetesTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AKubernetesTestGameMode::AKubernetesTestGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AKubernetesTestHUD::StaticClass();
}
